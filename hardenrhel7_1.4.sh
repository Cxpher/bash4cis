#!/bin/bash

echo
echo "***********************************************************************"
echo "***********************************************************************"
echo "***********************************************************************"
echo "*********************Author : V.Sathiesh Sean *************************"
echo "*********************Owner : V.Sathiesh Sean **************************"
echo "*****************Company : Cxrus Solutions Pte Ltd ********************"
echo "*************This script is the property of the author*****************"
echo "***It must not be used without the explicit permission of the author***"
echo "***********************************************************************"
echo "***********************************************************************"
echo "***********************************************************************"
echo 

echo
echo "Beginning hardening procedure.."
echo

sleep 5

# Variables

##############

grubuser=cxrus
grubencpw_fake="$(echo -e "cxrus!qwerty7\ncxrus!qwerty7" | grub2-mkpasswd-pbkdf2 2>/dev/null | tail --lines=1)"
grubencpw_true="$(echo -e "$grubencpw_fake" | sed 's/.*is //g')"
rsyslogip="192.168.50.38:8443"
ntpserver0="server 192.168.50.8 iburst"
ntpserver1=
ntpserver2=
ntpserver3=

##############

echo 
echo "Checking if leftovers exist from a previous run.."
echo
if ls /root/harden.log 1> /dev/null 2>&1; then 
	echo 
	echo "Previous run log exists.. backing up to /root/harden.log.bkp"
	echo 
	mv /root/harden.log /root/harden.bkp
	echo
	echo "Backup complete!"
	echo
	echo
	echo "Creating new log file @ /root/harden.log"
	echo
	touch /root/harden.log
	chmod 600 /root/harden.log
	echo
	echo "New log file creation complete.. continuing.."
	echo
else
	echo
	echo "Not run previously.."
	echo "Creating new log file @ /root/harden.log"
	echo
	touch /root/harden.log
	chmod 600 /root/harden.log
	echo
	echo "Log file creation complete.. continuing.."
	echo
fi

# Simple OS check. Will exit if it's not a RHEL 7 based distribution as this script is only for RHEL 7.x

echo	
echo	"Checking OS version.."
echo	

if grep -q -i "release 7" /etc/redhat-release; then 
	DistroBasedOn='Red Hat Enterprise Linux'
	DIST=`cat /etc/redhat-release |sed s/.*release\ // | sed s/\ .*//`
	echo
	echo OS version is $DistroBasedOn $DIST and is supported! continuing..
	echo
else
	echo
	echo "Incorrect OS version! Only RHEL 7.x is supported.. exiting.."
	echo
	exit 1
fi

echo 
echo "Blacklisting unnecessary filesystems"
echo 

# Create file for blacklisting of unnecessary filesystems if it does not exist and ignore if it already does

echo 
echo "Creating blacklist file CIS.conf.."
echo

if ls /etc/modprobe.d/CIS.conf 1> /dev/null 2>&1; then 
	echo
	echo "File /etc/modprobe.d/CIS.conf already exists.. skipping creation of file again" | tee -a /root/harden.log
	echo
else
	touch /etc/modprobe.d/CIS.conf 
	echo 
	echo "Blacklist file created"
	echo """"""""
fi

# Check if a filesystem exists before adding to the blacklist

if [[ $(lsmod | grep cramfs) ]]; then
	echo "install cramfs /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep freevxfs) ]]; then
	echo "install freevxfs /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep jffs2) ]]; then
	echo "install jffs2 /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep hfs) ]]; then
	echo "install hfs /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep hfsplus) ]]; then
	echo "install hfsplus /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep squashfs) ]]; then
	echo "install squashfs /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

if [[ $(lsmod | grep udf) ]]; then
	echo "install udf /bin/true" >> /etc/modprobe.d/CIS.conf 
else
	:
fi

# Exception made to allow vfat

#if [[ $(lsmod | grep vfat) ]]; then
#	echo "install vfat /bin/true" >> /etc/modprobe.d/CIS.conf 
#else
#	:
#fi

echo
echo "Unnecessary filesystems blacklisted successfully!"
echo

# Ensure systemd is mounting /tmp

if [[ $(mount | grep /tmp) ]]; then
	:
else
	echo
	echo "WARNING! Dedicated /tmp does not exist!" | tee -a /root/harden.log
	echo
fi
systemctl unmask tmp.mount
systemctl enable tmp.mount &> /dev/null
	echo
	echo "Creating file /etc/systemd/system/local-fs.target.wants/tmp.mount.."
	echo
if ls /etc/systemd/system/local-fs.target.wants/tmp.mount 1> /dev/null 2>&1; then 
	echo
	echo "File /etc/systemd/system/local-fs.target.wants/tmp.mount already exists.. skipping creation of file again" | tee -a /root/harden.log
	echo
else
	touch /etc/systemd/system/local-fs.target.wants/tmp.mount
cat <<EOT > /etc/systemd/system/local-fs.target.wants/tmp.mount
[Mount]
What=tmpfs
Where=/tmp
Type=tmpfs
Options=mode=1777,strictatime,noexec,nodev,nosuid
EOT
	echo
	echo "File created. Do not be alarmed if you see that it already exists.. this is normal"
	echo
fi

echo
echo "Checking if main partitions are proper devices.."
echo

# Check if /var is mounted as a separate partition

if mount -t "$(grep -v '^nodev' /proc/filesystems | cut -f2 | paste -s -d ,)" | grep /var 1> /dev/null 2>&1; then 
	echo "/var exists" | tee -a /root/harden.log
else 
	echo "/var does not exist as a physical or lvm drive partition" | tee -a /root/harden.log 
fi

# Check if /var/tmp is mounted as a separate partition

if mount -t "$(grep -v '^nodev' /proc/filesystems | cut -f2 | paste -s -d ,)" | grep /var/tmp 1> /dev/null 2>&1; then 
	echo "/var/tmp exists" | tee -a /root/harden.log
else 
	echo "/var/tmp does not exist as a physical or lvm drive partition" | tee -a /root/harden.log 
fi

# Check if /var/log is mounted as a separate partition 

if mount -t "$(grep -v '^nodev' /proc/filesystems | cut -f2 | paste -s -d ,)" | grep /var/log 1> /dev/null 2>&1; then 
	echo "/var/log exists" | tee -a /root/harden.log
else 
	echo "/var/log does not exist as a physical or lvm drive partition" | tee -a /root/harden.log 
fi

# Check if /var/log/audit is mounted as a separate partition

if mount -t "$(grep -v '^nodev' /proc/filesystems | cut -f2 | paste -s -d ,)" | grep /var/log/audit 1> /dev/null 2>&1; then 
	echo "/var/log/audit exists" | tee -a /root/harden.log
else 
	echo "/var/log/audit does not exist as a physical or lvm drive partition" | tee -a /root/harden.log 
fi

# Check if /home is mounted as a separate partition

if mount -t "$(grep -v '^nodev' /proc/filesystems | cut -f2 | paste -s -d ,)" | grep /home 1> /dev/null 2>&1; then 
	echo "/home exists" | tee -a /root/harden.log
else 
	echo "/home does not exist as a physical or lvm drive partition" | tee -a /root/harden.log 
fi

echo
echo "Check is completed! Check /root/harden.log for results.."
echo

# To be manually added into fstab!!
	# Ensure nodev,nosuid & noexec  options are factored in for /var/tmp mounting persistently
	# Ensure nodev option is factored in for /home mounting persistently
	# Ensure nodev,nosuid & noexec  options are factored in for /dev/shm mounting persistently
	# Ensure nodev,nosuid & noexec  options are factored in for all removable partitions persistently

# Check to ensure that are no world writable files and/or folders and if there are, set the sticky bit on them

echo 
echo "Checking for world writable files and/or folders.."
echo
world=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null)
if [ -z "$world" ]; then
	echo
	echo "No world writable files and/or folders exist.."
	echo
else
	echo
	echo "World writable files and/or folders exist!!"
	echo
	echo
	echo "Writing information on world writable files and/or folders into /root/world.log" 
	if ls /root/world.log 1> /dev/null 2>&1; then
		echo
        	echo "Previous world.log exists.. backing up to /root/world.log.bkp"
		echo
		mv /root/world.log /root/world.log.bkp
        	echo
		echo "Backup complete!"
		echo
		echo
		echo "Creating new world.log file @ /root/world.log"
        	echo
		touch /root/world.log
        	chmod 600 /root/world.log
		echo $world > /root/world.log
		echo
       		echo "New world.log file creation complete.. continuing.."
		echo "Written information on world writable files and/or folders into /root/world.log"	
		echo
			else
				echo
				echo "No previous world.log exists.."
				echo "Creating world.log.."
				echo $world > /root/world.log
				echo "Creation complete!"
				echo
				echo "Written information on world writable files and/or folders into /root/world.log"
				echo
				echo "Setting sticky bit on these world writable folders that were discovered.."
				df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d -perm -0002 2>/dev/null | xargs chmod a+t
				echo
				echo "Done!"
				echo	
	fi	
fi 

# Ensuring autofs is disabled

echo
echo "Disabling autofs service"
echo
if systemctl list-unit-files --state=enabled -all | grep autofs 1> /dev/null 2>&1; then 
	systemctl disable autofs
else 
	echo
	echo "Service autofs is already disabled or not present.."
	echo
fi

# Making sure gpgcheck is set to 1

echo
echo "Ensure that gpgcheck is set to 1 in yum.conf.. note that the default is 1.. if it has been changed to 0, i will change it back to 1.. however, i am stupid so i might not be able to react to other deviations yet.."
echo
gpgstatus="gpgcheck=0"
if ( `grep -Fxq "$gpgstatus" /etc/yum.conf` ); then
	sed -i -e 's/gpgcheck=0/gpgcheck=1/g' /etc/yum.conf
	echo 
	echo "It was set to 0.. as in disabled and i've set it back to 1.. as in enabled" | tee -a /harden.log
	echo
elif [[ -z `egrep -e "gpgcheck=" /etc/yum.conf 2> /dev/null 2>&1` ]]; then
	echo
	echo "gpgcheck=1" >> /etc/yum.conf 
	echo "Gpgcheck variable was not even present in yum.conf but i've enabled it.. GPG status is now okay" | tee -a /harden.log
	echo
else
	echo
	echo "GPG status is already okay"
	echo
fi

# Ensuring rhnsd is disabled (Note that running service has to still be stopped manually or rebooted -- For now - CBL)

echo
echo "Disabling rhnsd service.."
echo
chkconfig rhnsd off
echo
echo "Service has been disabled if not already.. no need to check before as chkconfig is absolute.."
echo

# Making sure AIDE is installed

echo
echo "Checking to see if AIDE is installed"
echo 
if [[ -z `rpm -qa aide` ]]; then
	echo
	echo "AIDE is not installed.. installing now.. (this will only work if you have a yum repository with AIDE in it.. i'm stupid that way..)"
	echo
	# Installation steps for AIDE + Setting cron job to run at midnight every day	
	yum -y install aide
	aide --init	
	mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
	crontab -r
	touch /root/cron.temp
	echo "00 00 * * * /usr/bin/aide --check" >> /root/cron.temp
	crontab /root/cron.temp
	rm /root/cron.temp
	echo
	echo "AIDE has been installed.."
	echo
else
	echo
	echo "AIDE is already present on the system.."
	echo
fi

# Secure Boot Settings - Part 1 (Part 2 - GRUB bootloader password to be set manually)

echo	
echo	"Setting grub permissions.."
echo	
	chown root:root /boot/grub2/grub.cfg
	chmod og-rwx /boot/grub2/grub.cfg	 
echo	
echo	"Done setting grub permissions.."
echo	

# Ensuring encrypted bootloader password is set

echo 
echo "Setting bootloader password hash for specified user.."
echo 

if [[ `egrep -e $grubuser /boot/grub2/grub.cfg` ]]; then
	echo 
	echo "The password hash for this user already exists in grub2.cfg!!" | tee -a /root/harden.log
	echo
else
echo -e "\ncat <<EOF
set superusers=$grubuser
password_pbkdf2 $grubuser $grubencpw_true
EOF" >> /etc/grub.d/01_users
	sleep 2
	grub2-mkconfig > /boot/grub2/grub.cfg 2> /dev/null 2>&1
fi

# Ensuring authentication required for single user mode

echo	
echo	"Ensuring single user mode requires authentication.."
echo	
	sed -i "s#/usr/sbin/sulogin#/sbin/sulogin/#" /usr/lib/systemd//system/rescue.service
	sed -i "s#/usr/sbin/sulogin#/sbin/sulogin/#" /usr/lib/systemd//system/emergency.service
echo	
echo	"Single user mode now requires authentication!"
echo	

# Ensuring core dumps are restricted - Part 1

echo
echo "Setting limit hard core 0 in limits.conf.."
echo
if [[ -z `egrep -e "* hard core 0" /etc/security/limits.conf 2> /dev/null 2>&1` ]]; then
	echo "* hard core 0" >> /etc/security/limits.conf
	echo
	echo "Done setting limit hard core 0 in limits.conf"
	echo
else
        echo
        echo "The hard core 0 limit already exists in limits.conf.." | tee -a /root/harden.log
	echo 
fi

# Ensuring core dumps are restricted - Part 2

echo
echo "Setting fs.suid_dumpable to disable.." 
echo
if [[ -z `egrep -e "fs.suid_dumpable = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
	echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
	echo
	echo "Done setting fs.suid_dumpable to disable"
	echo
else
        echo 
        echo "fs.suid_dumpable already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring XD/NX support is enabled 

echo 
echo "Checking for XD/NX Protection.."
echo
if ( `dmesg | grep -q "^NX (Execute Disable) protection: active"` ); then
	echo
	echo "XD/NX Protection not active!!! WARNING!" | tee -a /root/harden.log
	echo
else
	echo
	echo "XD/NX Protection is active"
	echo
fi

# Ensuring address space layout randomization (ASLR) is enabled

echo
echo "Setting address space layout randomization" 
echo
if [[ -z `egrep -e "kernel.randomize_va_space = 2" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "kernel.randomize_va_space = 2" >> /etc/sysctl.conf
        echo
        echo "Done setting setting address space layout randomization"
        echo
else
        echo 
        echo "kernel.randomize_va_space already exists in sysctl.conf and is set to 2" | tee -a /root/harden.log
        echo
fi

# Ensuring prelink is disabled
	
	echo 
	echo "Checking to make sure prelink is not installed.."
	echo
	sleep 2
if [[ -z `rpm -qa prelink` ]]; then
        echo
        echo "Prelink is not installed"
        echo
else
	echo
	echo "Prelink is installed.. removing.." | tee -a /root/harden.log
	echo 
	prelink -ua
	sleep 5
	yum -y remove prelink
	echo
	echo "Prelink has been successfully removed!" | tee -a /root/harden.log
fi

# Ensuring grub does not have selinux disabled

echo    
echo    "Backing up grub config first.."
echo
        cp -f /etc/default/grub /etc/default/grub.bkp
echo
echo    "Grub config backed up"
echo
echo
echo    "Checking if selinux is disabled via grub.."
echo    
if [[ -z `egrep -e "selinux=0" /boot/grub2/grub.cfg 2> /dev/null 2>&1` ]]; then 
	echo
	echo "Grub does not disable selinux"
        echo
else 
	echo 
        echo "Selinux is disabled via grub.." | tee -a /root/harden.log
        echo
        echo
        echo "Enabling selinux again and making sure grub dosen't disabled it.."
        echo
        sed -i 's/selinux=0//g' /etc/default/grub 2> /dev/null 2>&1
        echo
        echo "Done!"
        echo
fi

echo
echo    "Checking if enforcing is disabled via grub.."
echo    
if [[ -z `egrep -e "enforcing=0" /boot/grub2/grub.cfg 2> /dev/null 2>&1` ]]; then
        echo
        echo "Grub does not disable enforcing"
        echo
else
        echo 
        echo "Enforcing is disabled via grub.." | tee -a /root/harden.log
        echo
        echo
        echo "Enabling enforcing again and making sure grub dosen't disabled it.."
        echo
        sed -i 's/enforcing=0//g' /etc/default/grub 2> /dev/null 2>&1
        echo
        echo "Done!"
        echo
fi

echo	
echo	"Re-compiling grub.."
echo
	sleep 2
        grub2-mkconfig > /boot/grub2/grub.cfg 2> /dev/null 2>&1
echo	
echo	"Done re-compiling grub :)"
echo

# Ensuring grub does not have selinux disabled

echo    
echo    "Backing up selinux config first.."
echo
        cp -f /etc/selinux/config /etc/selinux/config.bkp
echo
echo    "Selinux config backed up"
echo
echo
echo    "Checking if selinux is enforcing.."
echo    
if [[ -z `egrep -e "SELINUX=enforcing" /etc/selinux/config 2> /dev/null 2>&1` ]]; then
	echo
	echo "Selinux is NOT enforcing!" | tee -a /root/harden.log
	echo
	echo
	echo "Taking remediation action.."
	echo
	sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config 2> /dev/null 2>&1
	sed -i 's/SELINUX=disabled/SELINUX=enforcing/g' /etc/selinux/config 2> /dev/null 2>&1
	echo 
	echo "Selinux has been set to enforce after a reboot - to ensure a relabel of the fs if it was in a fully disabled state"
	echo
else
	echo 
	echo "Selinux is set to enforcing already so no remediation action needed"
	echo
fi 

# Ensuring selinux policy is configured correctly

echo
echo    "Checking if selinux policy is set to targeted"
echo    
if [[ -z `egrep -e "SELINUXTYPE=targeted" /etc/selinux/config 2> /dev/null 2>&1` ]]; then
	echo
	echo "Selinux policy is not set correctly!" | tee -a /root/harden.log
	echo
	echo
	echo "Taking remediation action.."
	echo
	sed -i 's/SELINUXTYPE=minimum/SELINUXTYPE=targeted/g' /etc/selinux/config 2> /dev/null 2>&1
	sed -i 's/SELINUXTYPE=mls/SELINUXTYPE=targeted/g' /etc/selinux/config 2> /dev/null 2>&1
	echo 
	echo "Selinux policy setting has been corrected"
	echo
else
	echo 
	echo "Selinux policy is set correctly to targeted"
	echo
fi

# Ensuring setroubleshoot is disabled

        echo 
        echo "Checking to make sure setroubleshoot is not installed.."
        echo
if [[ -z `rpm -qa setroubleshoot` ]]; then
        echo
        echo "Setroubleshoot is not installed"
        echo
else
        echo
        echo "Setroubleshoot is installed.. removing.." | tee -a /root/harden.log
        echo 
        yum -y remove setroubleshoot
        sleep 2
	echo
        echo "Setroubleshoot has been successfully removed!" | tee -a /root/harden.log
	echo
fi
 
# Ensuring mcstrans is disabled

        echo 
        echo "Checking to make sure mcstrans is not installed.."
        echo
if [[ -z `rpm -qa mcstrans` ]]; then
        echo
        echo "Mcstrans is not installed"
        echo
else
        echo
        echo "Mcstrans is installed.. removing.." | tee -a /root/harden.log
        echo 
        yum -y remove mcstrans
        sleep 2
	echo
        echo "Mcstrans has been successfully removed!" | tee -a /root/harden.log
fi

# Checking to see if any unconfined daemons are running

echo
echo    "Checking if any unconfined daemons are running.. will log a message to inform if there are to /root/harden.log"
echo    
if [[ -z `ps -eZ | egrep "initrc" | egrep -vw "tr|ps|egrep|bash|awk" | tr ':' ' ' | awk '{ print $NF }' 2> /dev/null 2>&1` ]]; then
        echo
        echo "No unconfined daemons are running"
        echo
else
	echo
	echo "Unconfined daemons are running!!!" | tee -a /root/harden.log
	echo
fi

# Checking to ensure that selinux is indeed installed and not a farce

	echo 
        echo "Checking to make sure selinux is really installed and not a farce.."
        echo
if [[ -z `rpm -qa libselinux` ]]; then
        echo
        echo "Selinux is not installed!! WHAT THE HELL!" | tee -a /root/harden.log
	echo
	echo "Installing now!!"
	echo
	yum -y install libselinux
	sleep 3
	echo
	echo "Selinux has now been installed by me.. rerun the hardening script again to ensure it's hardened properly" | tee -a /root/harden.log
        echo
else
        echo
        echo "Selinux is installed truly.. no cause for concern"
        echo 
fi

# Ensuring /etc/motd has the relevant policy message

echo
echo	"Overwriting /etc/motd with relevant policy message"
echo 
cat <<EOT > /etc/motd
Authorised Users Only
Use of this network is restricted to authorised users only.  User activity may be monitored and/or recorded. Anyone using this network expressly consents to such monitoring and/or recording.  BE ADVISED: if possible criminal activity is detected, these records, along with certain personal information, may be provided to law enforcement officials.
EOT
echo
echo    "Done inserting policy message into /etc/motd"
echo

# Ensuring /etc/issue has the relevant policy message

echo
echo    "Overwriting /etc/issue with relevant policy message"
echo 
cat <<EOT > /etc/issue
Authorised Users Only
Use of this network is restricted to authorised users only.  User activity may be monitored and/or recorded. Anyone using this network expressly consents to such monitoring and/or recording.  BE ADVISED: if possible criminal activity is detected, these records, along with certain personal information, may be provided to law enforcement officials.
EOT
echo
echo    "Done inserting policy message into /etc/issue"
echo 

# Ensuring /etc/issue.net has the relevant policy message

echo
echo    "Overwriting /etc/issue.net with relevant policy message"
echo 
cat <<EOT > /etc/issue.net
Authorised Users Only
Use of this network is restricted to authorised users only.  User activity may be monitored and/or recorded. Anyone using this network expressly consents to such monitoring and/or recording.  BE ADVISED: if possible criminal activity is detected, these records, along with certain personal information, may be provided to law enforcement officials.
EOT
echo
echo    "Done inserting policy message into /etc/issue.net"
echo 

# Ensuring corret permissions on files set with policy message

echo
echo    "Setting correct permissions for motd, issue and issue.net"
echo 
chown root:root /etc/motd /etc/issue /etc/issue.net
chmod 644 /etc/motd /etc/issue /etc/issue.net
echo
echo	"Correct permissions have been set on these files"
echo

# Ensuring unnecessary services like chargen-dgram and chargen-stream are turned off if present

echo
echo "Turning off xinetd based subservices"
echo
chkconfig chargen-dgram off &> /dev/null
chkconfig chargen-stream off &> /dev/null 
chkconfig daytime-dgram off &> /dev/null 
chkconfig daytime-stream off &> /dev/null 
chkconfig discard-dgram off &> /dev/null 
chkconfig discard-stream off &> /dev/null 
chkconfig "echo-dgram" off &> /dev/null 
chkconfig "echo-stream" off &> /dev/null 
chkconfig "time-dgram" off &> /dev/null 
chkconfig "time-stream" off &> /dev/null 
chkconfig tftp off &> /dev/null
echo 
echo "Done turning off these services"
echo

# Ensuring xinetd itself is disabled if present

echo
echo "Disabling xinetd service itself"
echo
if systemctl list-unit-files --state=enabled -all | grep xinetd &> /dev/null 2>&1; then
	systemctl disable xinetd
else
        echo
        echo "Service xinetd is already disabled or not present.."
        echo
fi

# Checking to ensure that ntp is installed

        echo 
        echo "Checking to make sure NTP is installed.."
        echo
if [[ -z `rpm -qa ntp` ]]; then
        echo
        echo "Ntp is not installed!" | tee -a /root/harden.log
	echo
	echo
        echo "Installing now!!"
        echo
	yum -y install ntp
        sleep 3
        echo
	echo "Ntp has now been installed by me.." | tee -a /root/harden.log
        echo
else
        echo
        echo "Ntp is already installed on the system"
        echo 
fi

# Ensure ntp is configured

        echo
        echo    "Backing up original ntp.conf to ntp.conf.bkp"
        echo
                cp -f /etc/ntp.conf /etc/ntp.conf.bkp
		cp -f /etc/sysconfig/ntpd /etc/sysconfig/ntpd.bkp
        echo
        echo    "Backup of ntp config complete"
        echo
        echo
        echo    "Setting ntp to sync time with ntp appliance"
        echo
        sed -i -e "s/server 0.rhel.pool.ntp.org iburst/$ntpserver0/g" /etc/ntp.conf
        sed -i -e "s/server 1.rhel.pool.ntp.org iburst/$ntpserver1/g" /etc/ntp.conf
        sed -i -e "s/server 2.rhel.pool.ntp.org iburst/$ntpserver2/g" /etc/ntp.conf
        sed -i -e "s/server 3.rhel.pool.ntp.org iburst/$ntpserver3/g" /etc/ntp.conf
	sed -i -e "s/restrict default nomodify notrap nopeer noquery/restrict -4 default kod nomodify notrap nopeer noquery/g" /etc/ntp.conf
	sleep 2
if [[ -z `egrep -e "restrict -6 default kod nomodify notrap nopeer noquery" /etc/ntp.conf 2> /dev/null 2>&1` ]]; then
	sed -i '/restrict -4 default kod nomodify notrap nopeer noquery/a \
restrict -6 default kod nomodify notrap nopeer noquery' /etc/ntp.conf
else
	:
fi
	sed -i -e 's/OPTIONS="-g"/OPTIONS="-u ntp:ntp"/g' /etc/sysconfig/ntpd
	echo
        echo    "Set ntp to sync time with ntp appliance"
        echo

# Ensuring avahi-daemon itself is disabled if present

echo
echo "Disabling avahi-daemon service"
echo
if systemctl list-unit-files --state=enabled -all | grep 'avahi-daemon' &> /dev/null 2>&1; then
        systemctl disable avahi-daemon
else
        echo
        echo "Service avahi-daemon is already disabled or not present.."
        echo
fi

# Ensuring cups itself is disabled if present

echo
echo "Disabling cups service"
echo
if systemctl list-unit-files --state=enabled -all | grep cups &> /dev/null 2>&1; then
        systemctl disable cups
else
        echo
        echo "Cups is already disabled or not present.."
        echo
fi

# Ensuring dhpcd itself is disabled if present

echo
echo "Disabling dhcpd service"
echo
if systemctl list-unit-files --state=enabled -all | grep dhcpd &> /dev/null 2>&1; then
        systemctl disable dhcpd
else
        echo
        echo "Dhcpd is already disabled or not present.."
        echo
fi

# Ensuring that LDAP itself is disabled if present

echo
echo "Disabling slapd service"
echo
if systemctl list-unit-files --state=enabled -all | grep slapd &> /dev/null 2>&1; then
        systemctl disable slapd
else
        echo
        echo "LDAP is already disabled or not present.."
        echo
fi

# Ensuring that NFS and RPC itself are disabled if present

#echo
#echo "Disabling nfs service"
#echo
#if systemctl list-unit-files --state=enabled -all | grep nfs &> /dev/null 2>&1; then
#        systemctl disable nfs
#else
#        echo
#        echo "Nfs is already disabled or not present.."
#        echo
#fi

#echo
#echo "Disabling rpcbind service"
#echo
#if systemctl list-unit-files --state=enabled -all | grep rpcbind &> /dev/null 2>&1; then
#        systemctl disable rpcbind
#else
#        echo
#        echo "Rpcbind is already disabled or not present.."
#        echo
#fi

echo
echo "Disabling DNS service"
echo
if systemctl list-unit-files --state=enabled -all | grep named &> /dev/null 2>&1; then
        systemctl disable named
else
        echo
        echo "DNS is already disabled or not present.."
        echo
fi

# Ensuring that vsftpd is disabled if present

echo
echo "Disabling vsftpd service"
echo
if systemctl list-unit-files --state=enabled -all | grep vsftpd &> /dev/null 2>&1; then
        systemctl disable vsftpd
else
        echo
        echo "Vsftpd is already disabled or not present.."
        echo
fi

# Ensuring that httpd itself is disabled if present - FLAG OUT THAT THIS IS NOT RECOMMENDED TO CUSTOMER
#
#echo
#echo "Disabling httpd service"
#echo
#if systemctl list-unit-files --state=enabled -all | grep httpd &> /dev/null 2>&1; then
#        systemctl disable httpd
#else
#        echo
#        echo "Httpd is already disabled or not present.."
#        echo
#fi

# Ensuring that dovecot is disabled if present

echo
echo "Disabling dovecot service"
echo
if systemctl list-unit-files --state=enabled -all | grep dovecot &> /dev/null 2>&1; then
        systemctl disable dovecot
else
        echo
        echo "Dovecot is already disabled or not present.."
        echo
fi

# Ensuring that samba is disabled if present

echo
echo "Disabling samba service"
echo
if systemctl list-unit-files --state=enabled -all | grep smb &> /dev/null 2>&1; then
        systemctl disable smb
else
        echo
        echo "Smb is already disabled or not present.."
        echo
fi

# Ensuring that squid is disabled if present

echo
echo "Disabling squid service"
echo
if systemctl list-unit-files --state=enabled -all | grep squid &> /dev/null 2>&1; then
        systemctl disable squid
else
        echo
        echo "Squid is already disabled or not present.."
        echo
fi

# Ensuring that snmpd itself is disabled if present - FLAG OUT THAT THIS IS NOT RECOMMENDED TO CUSTOMER
#
#echo
#echo "Disabling snmpd service"
#echo
#if systemctl list-unit-files --state=enabled -all | grep snmpd &> /dev/null 2>&1; then
#        systemctl disable snmpd
#else
#        echo
#        echo "Snmpd is already disabled or not present.."
#        echo
#fi

# Quick and dirty check to see if "inet_interfaces = localhost" is present in /etc/postfix/main.cf 

echo
echo "Quick sanity check of postfix MTA config to see if it only listens on loop back"
echo
if [[ -z `egrep -e "inet_interfaces = localhost" /etc/postfix/main.cf 2> /dev/null 2>&1` ]]; then
	echo
	echo "DANGER. CHECK /etc/postfix/main.cf" | tee -a /root/harden.log
	echo
else
	echo
	echo "MTA is listening only on loopback and is fine"
	echo 
fi

# Ensuring that NIS is disabled if present

echo
echo "Disabling NIS service"
echo
if systemctl list-unit-files --state=enabled -all | grep ypserv &> /dev/null 2>&1; then
        systemctl disable ypserv
else
        echo
        echo "NIS is already disabled or not present.."
        echo
fi

# Ensuring that rsh and it's subservices are completely disabled if present

echo
echo "Disabling rsh.socket"
echo
if systemctl list-unit-files --state=enabled -all | grep rsh.socket &> /dev/null 2>&1; then
        systemctl disable rsh.socket
else
        echo
        echo "Rsh.socket disabled or not present.."
        echo
fi

echo
echo "Disabling rlogin.socket"
echo
if systemctl list-unit-files --state=enabled -all | grep rlogin.socket &> /dev/null 2>&1; then
        systemctl disable rlogin.socket
else
        echo
        echo "Rlogin.socket disabled or not present.."
        echo
fi

echo
echo "Disabling rexec.socket"
echo
if systemctl list-unit-files --state=enabled -all | grep rexec.socket &> /dev/null 2>&1; then
        systemctl disable rexec.socket
else
        echo
        echo "Rexec.socket disabled or not present.."
        echo
fi

# Ensuring that ntalk is disabled if present

echo
echo "Disabling ntalk"
echo
if systemctl list-unit-files --state=enabled -all | grep ntalk &> /dev/null 2>&1; then
        systemctl disable ntalk
else
        echo
        echo "Ntalk disabled or not present.."
        echo
fi

# Ensuring that telnet.socket service is disabled if present

echo
echo "Disabling telnet.socket"
echo
if systemctl list-unit-files --state=enabled -all | grep telnet.socket &> /dev/null 2>&1; then
        systemctl disable telnet.socket
else
        echo
        echo "telnet.socket disabled or not present.."
        echo
fi

# Ensuring that tftp.socket service (different from xinetd tftp) is disabled if present

echo
echo "Disabling tftp.socket"
echo
if systemctl list-unit-files --state=enabled -all | grep tftp.socket &> /dev/null 2>&1; then
        systemctl disable tftp.socket
else
        echo
        echo "tftp.socket disabled or not present.."
        echo
fi

# Ensuring that rsyncd service is disabled if present

echo
echo "Disabling rsyncd"
echo
if systemctl list-unit-files --state=enabled -all | grep rsyncd &> /dev/null 2>&1; then
        systemctl disable rsyncd
else
        echo
        echo "Rsyncd disabled or not present.."
        echo
fi

# Ensuring ypbind (NIS client) is disabled

        echo 
        echo "Checking to make sure ypbind is not installed.."
        echo
        sleep 2
if [[ -z `rpm -qa ypbind` ]]; then
        echo
        echo "Ypbind is not installed"
        echo
else
        echo
        echo "Ypbind is installed.. removing.." | tee -a /root/harden.log
        echo 
        sleep 2
        yum -y remove ypbind
        echo
        echo "Ypbind has been successfully removed!" | tee -a /root/harden.log
fi

# Ensuring rsh client is disabled

	echo 
        echo "Checking to make sure rsh client is not installed.."
        echo
        sleep 2
if [[ -z `rpm -qa rsh` ]]; then
        echo
        echo "Rsh client is not installed"
        echo
else
        echo
        echo "Rsh client is installed.. removing.." | tee -a /root/harden.log
        echo 
        sleep 2
        yum -y remove rsh
        echo
        echo "Rsh client has been successfully removed!" | tee -a /root/harden.log
fi

# Ensure talk client is disabled

	echo 
        echo "Checking to make sure talk client is not installed.."
        echo
        sleep 2
if [[ -z `rpm -qa talk` ]]; then
        echo
        echo "Talk client is not installed"
        echo
else
        echo
        echo "Talk client is installed.. removing.." | tee -a /root/harden.log
        echo 
        sleep 2
        yum -y remove talk
        echo
        echo "Talk has been successfully removed!" | tee -a /root/harden.log
fi

# Ensure telnet client is disabled

	echo 
        echo "Checking to make sure telnet client is not installed.."
        echo
        sleep 2
if [[ -z `rpm -qa telnet` ]]; then
        echo
        echo "Telnet client is not installed"
        echo
else
        echo
        echo "Telnet client is installed.. removing.." | tee -a /root/harden.log
        echo 
        sleep 2
        yum -y remove telnet
        echo
        echo "Telnet client has been successfully removed!" | tee -a /root/harden.log
fi

# Ensure ldap client is disabled

	echo 
        echo "Checking to make sure ldap client is not installed.."
        echo
        sleep 2
if [[ -z `rpm -qa openldap-clients` ]]; then
        echo
        echo "Openldap-clients is not installed"
        echo
else
        echo
        echo "Openldap-clients is installed.. removing.." | tee -a /root/harden.log
        echo 
        sleep 2
        yum -y remove openldap-clients
        echo
        echo "Openldap-clients has been successfully removed!" | tee -a /root/harden.log
fi

# Ensuring ipv4 forwarding is disabled

echo
echo "Setting ip forwarding to disable.." 
echo
if [[ -z `egrep -e "net.ipv4.ip_forward = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.ip_forward = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting ip forwarding to disable"
        echo
else
        echo 
        echo "Ip forwarding already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring packet redirect sending is disabled

echo
echo "Setting packet redirect sending to disable.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.send_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all redirects to disable"
        echo
else
        echo 
        echo "All redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

if [[ -z `egrep -e "net.ipv4.conf.default.send_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.send_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default redirects to disable"
        echo
else
        echo 
        echo "Default redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring source routed packets are not accepted

echo
echo "Setting all source routed packets to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.accept_source_route = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all accept source routed packets to disabled"
        echo
else
        echo 
        echo "All accept source routed packets already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default source routed packets to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.default.accept_source_route = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default accept source routed packets to disabled"
        echo
else
        echo 
        echo "Default accept source routed packets already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring ICMP redirects are not accepted

echo
echo "Setting all ICMP redirects to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.accept_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all ICMP redirects to disabled"
        echo
else
        echo 
        echo "All ICMP redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default ICMP redirects to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.default.accept_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default ICMP redirects to disabled"
        echo
else
        echo 
        echo "Default ICMP redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring secure ICMP redirects are not accepted

echo
echo "Setting all secure ICMP redirects to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.secure_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.secure_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all secure ICMP redirects to disabled"
        echo
else
        echo 
        echo "All secure ICMP redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default secure ICMP redirects to reject.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.default.secure_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.secure_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default secure ICMP redirects to disabled"
        echo
else
        echo 
        echo "Default secure ICMP redirects already exists in sysctl.conf and is set to be disabled" | tee -a /root/harden.log
        echo
fi

# Ensuring suspicious packets are logged

echo
echo "Setting all suspicious packets to be logged.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.log_martians = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting all suspicious packets to be logged"
        echo
else
        echo 
        echo "All suspicious packets logging already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default suspicious packets to be logged.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.default.log_martians = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.log_martians = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting default suspicious packets to be logged"
        echo
else
        echo 
        echo "Default suspicious packets logging already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

# Ensuring broadcast ICMP requests are ignored

echo
echo "Setting broadcast ICMP requests to be ignored.." 
echo
if [[ -z `egrep -e "net.ipv4.icmp_echo_ignore_broadcasts = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting broadcast ICMP requests to be inored"
        echo
else
        echo 
        echo "Broadcast ICMP request ignoring already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

# Ensuring bogus ICMP requests are ignored

echo
echo "Setting bogus ICMP requests to be ignored.." 
echo
if [[ -z `egrep -e "net.ipv4.icmp_ignore_bogus_error_responses = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting bogus ICMP requests to be inored"
        echo
else
        echo 
        echo "Bogus ICMP request ignoring already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

# Ensuring reverse path filtering is enabled

echo
echo "Setting all reverse path filtering to be enabled.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.all.rp_filter = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.all.rp_filter = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting all reverse path filtering to be enabled"
        echo
else
        echo 
        echo "All reverse path filtering already exists in sysctl.conf and is enabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default reverse path filtering to be enabled.." 
echo
if [[ -z `egrep -e "net.ipv4.conf.default.rp_filter = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.conf.default.rp_filter = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting default reverse path filtering to be enabled"
        echo
else
        echo 
        echo "Default reverse path filtering already exists in sysctl.conf and is enabled" | tee -a /root/harden.log
        echo
fi

# Ensuring TCP SYN cookies are enabled

echo
echo "Setting TCP SYN cookies to be enabled.." 
echo
if [[ -z `egrep -e "net.ipv4.tcp_syncookies = 1" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
        echo
        echo "Done setting TCP SYN cookies to be enabled"
        echo
else
        echo 
        echo "TCP SYN cookies already exists in sysctl.conf and is enabled" | tee -a /root/harden.log
        echo
fi

# Ensuring IPv6 router advertisements are not accepted

echo
echo "Setting all IPv6 router advertisements to be rejected.." 
echo
if [[ -z `egrep -e "net.ipv6.conf.all.accept_ra = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv6.conf.all.accept_ra = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all IPv6 router advertisements to be rejected"
        echo
else
        echo 
        echo "All IPv6 router advertisement rejection already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default IPv6 router advertisements to be rejected.." 
echo
if [[ -z `egrep -e "net.ipv6.conf.default.accept_ra = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv6.conf.default.accept_ra = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default IPv6 router advertisements to be rejected"
        echo
else
        echo 
        echo "Default IPv6 router advertisement rejection already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

# Ensuring IPv6 redirects are not accepted

echo
echo "Setting all IPv6 redirects to be rejected.." 
echo
if [[ -z `egrep -e "net.ipv6.conf.all.accept_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv6.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting all IPv6 redirects to be rejected"
        echo
else
        echo 
        echo "All IPv6 redirects rejection already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

echo
echo "Setting default IPv6 redirects to be rejected.." 
echo
if [[ -z `egrep -e "net.ipv6.conf.default.accept_redirects = 0" /etc/sysctl.conf 2> /dev/null 2>&1` ]]; then
        echo "net.ipv6.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
        echo
        echo "Done setting default IPv6 redirects to be rejected"
        echo
else
        echo 
        echo "Default IPv6 redirects rejection already exists in sysctl.conf" | tee -a /root/harden.log
        echo
fi

# Ensuring IPv6 is disabled

echo
echo "Disabling IPv6.." 
echo
if [[ -z `egrep -e "options ipv6 disable=1" /etc/modprobe.d/CIS.conf 2> /dev/null 2>&1` ]]; then
        echo "options ipv6 disable=1" >> /etc/modprobe.d/CIS.conf
        echo
        echo "Set IPv6 to be disabled successfully"
        echo
else
        echo 
        echo "IPv6 already disabled" | tee -a /root/harden.log
        echo
fi

# Checking to ensure that TCP Wrappers is installed (note that this should also installed dependency tcp_wrappers-libs)

        echo 
        echo "Checking to make sure TCP Wrappers is installed.."
        echo
if [[ -z `rpm -qa tcp_wrappers` ]]; then
        echo
        echo "TCP Wrappers is not installed!" | tee -a /root/harden.log
        echo
        echo
        echo "Installing now!!"
        echo
        yum -y install tcp_wrappers 
        sleep 3
        echo
        echo "TCP Wrappers has now been installed by me.." | tee -a /root/harden.log
        echo
else
        echo
        echo "TCP Wrappers is already installed on the system"
        echo 
fi

# Create hosts.allow - Check with customer what they wanna do with this

# Create hosts.deny - Check with customer if they wanna enable this

#echo
#echo "Denying all hosts.." 
#echo
#if [[ -z `egrep -e "ALL: ALL" /etc/hosts.deny 2> /dev/null 2>&1` ]]; then
#        echo "ALL: ALL" >> /etc/hosts.deny
#        echo
#        echo "ALL HOSTS ARE NOW DENIED by default!!!"
#        echo
#else
#        echo 
#        echo "All hosts are already denied by default for hosts.deny" | tee -a /root/harden.log
#        echo
#fi

# Setting permissions on /etc/hosts.allow

	echo
	echo "Setting correct permissions on /etc/hosts.allow file"
        echo
        chown root:root /etc/hosts.allow
        chmod 644 /etc/hosts.allow
        echo
        echo "Correct permissions set on /etc/hosts.allow"
        echo

	echo
        echo "Setting correct permissions on /etc/hosts.deny file"
        echo
        chown root:root /etc/hosts.deny
        chmod 644 /etc/hosts.deny
        echo
        echo "Correct permissions set on /etc/hosts.deny"
        echo

# Disabling uncommon network protocols

echo
echo "Disabling dccp protocol.." 
echo
if [[ -z `egrep -e "install dccp /bin/true" /etc/modprobe.d/CIS.conf 2> /dev/null 2>&1` ]]; then
        echo "install dccp /bin/true" >> /etc/modprobe.d/CIS.conf
        echo
        echo "Disabled dccp protocol successfully"
        echo
else
        echo 
        echo "Dccp protocol already disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Disabling sctp protocol.." 
echo
if [[ -z `egrep -e "install sctp /bin/true" /etc/modprobe.d/CIS.conf 2> /dev/null 2>&1` ]]; then
        echo "install sctp /bin/true" >> /etc/modprobe.d/CIS.conf
        echo
        echo "Disabled sctp protocol successfully"
        echo
else
        echo 
        echo "Sctp protocol already disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Disabling rds protocol.." 
echo
if [[ -z `egrep -e "install rds /bin/true" /etc/modprobe.d/CIS.conf 2> /dev/null 2>&1` ]]; then
        echo "install rds /bin/true" >> /etc/modprobe.d/CIS.conf
        echo
        echo "Disabled rds protocol successfully"
        echo
else
        echo 
        echo "Rds protocol already disabled" | tee -a /root/harden.log
        echo
fi

echo
echo "Disabling tipc protocol.." 
echo
if [[ -z `egrep -e "install tipc /bin/true" /etc/modprobe.d/CIS.conf 2> /dev/null 2>&1` ]]; then
        echo "install tipc /bin/true" >> /etc/modprobe.d/CIS.conf
        echo
        echo "Disabled tipc protocol successfully"
        echo
else
        echo 
        echo "Tipc protocol already disabled" | tee -a /root/harden.log
        echo
fi

# Checking to ensure that iptables is installed

        echo 
        echo "Checking to make sure iptables is installed.."
        echo
if [[ -z `rpm -qa iptables` ]]; then
        echo
        echo "Iptables is not installed!" | tee -a /root/harden.log
        echo
        echo
        echo "Installing now!!"
        echo
        yum -y install iptables
        sleep 3
        echo
        echo "Iptables has now been installed by me.." | tee -a /root/harden.log
        echo
else
        echo
        echo "Iptables is already installed on the system"
        echo 
fi

# Ensuring that audidt service is enabled if present

echo
echo "Enabling auditd service"
echo
if systemctl list-unit-files --state=enabled -all | grep auditd &> /dev/null 2>&1; then
        systemctl enable auditd
else
        echo
        echo "Auditd service is already enabled"
        echo
fi

# Ensuring that services that start before auditd starts are also audited

echo
echo    "Ensuring that services that start before auditd starts are also audited.."
echo    
if [[ -z `egrep -e "audit=1" /boot/grub2/grub.cfg 2> /dev/null 2>&1` ]]; then
        echo
        echo "Grub does not enable auditing at boot! ENABLING NOW!" | tee -a /root/harden.log
	echo
        sed -i "s/GRUB_CMDLINE_LINUX=\"\(.*\)\"/GRUB_CMDLINE_LINUX=\"\1 audit=1\"/" /etc/default/grub 2> /dev/null 2>&1        
	echo
	echo "Auditd will now be auditing services even at boot before it starts.."
	echo
else
	echo 
	echo "Auditd is already set to do this"
	echo
fi

echo    
echo    "But before that can happen for sure, best to re-compile grub.."
echo
        sleep 2
        grub2-mkconfig > /boot/grub2/grub.cfg 2> /dev/null 2>&1
echo    
echo    "Done re-compiling grub :>>"
echo

# Ensuring events that modify date and time are collected

echo    
echo    "Backing up audit.rules file first.."
echo
        cp -f /etc/audit/audit.rules /etc/audit/audit.rules
echo
echo    "Audit.rules backed up"
echo

echo 
echo	"Checking to ensure events that modify date and time are collected.."
echo 
if [[ -z `egrep -e "adjtimex" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo	"Warning.. events that modify date and time are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo	"Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always, exit -F arch=b64 -S adjtimex -S settimeofday -k time-change
-a always, exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change
-a always, exit -F arch=b64 -S clock_settime -k time-change
-a always, exit -F arch=b32 -S clock_settime -k time-change
-w /etc/localtime -p wa -k time-change
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Events that modify date and time are already being collected"
	echo
fi

# Ensuring events that modify user/group information are collected

	echo 
	echo    "Checking to ensure events that modify user/group information are collected.."
	echo 
if [[ -z `egrep -e "-w /etc/group -p wa -k identity" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. events that modify user/group information are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /etc/group -p wa -k identity
-w /etc/passwd -p wa -k identity
-w /etc/gshadow -p wa -k identity
-w /etc/shadow -p wa -k identity
-w /etc/security/opasswd -p wa -k identity 
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Events that modify user/group information are already being collected"
	echo
fi

# Ensuring events that modify network environment are collected

	echo 
	echo    "Checking to ensure events that modify network environment are collected.."
	echo 
if [[ -z `egrep -e "setdomainname" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such events that modify network environment are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always,exit -F arch=b64 -S sethostname -S setdomainname -k system-locale
-a always,exit -F arch=b32 -S sethostname -S setdomainname -k system-locale
-w /etc/issue -p wa -k system-locale
-w /etc/issue.net -p wa -k system-locale
-w /etc/hosts -p wa -k system-locale
-w /etc/sysconfig/network -p wa -k system-locale
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Events that modify network environment are already being collected"
	echo
fi

# Ensuring events that modify the Mandatory Access Control (selinux) of the system are collected

	echo 
	echo    "Checking to ensure events that modify Mandatory AC are collected.."
	echo 
if [[ -z `egrep -e "-w /etc/selinux/ -p wa -k MAC-policy" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such events that modify Mandatory AC are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /etc/selinux/ -p wa -k MAC-policy
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Events that modify Mandatory AC are already being collected"
	echo
fi

# Ensuring login/logout events are collected

	echo 
	echo    "Checking to ensure login/logout events are collected.."
	echo 
if [[ -z `egrep -e "-w /var/log/lastlog -p wa -k logins" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such login/logout events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /var/log/lastlog -p wa -k logins
-w /var/run/faillock/ -p wa -k logins
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Events related to login/logout are already being collected"
	echo
fi

# Ensuring session initiation information is collected

	echo 
	echo    "Checking to ensure session initiation information collected.."
	echo 
if [[ -z `egrep -e "-w /var/run/utmp -p wa -k session" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such session initiation information is currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /var/run/utmp -p wa -k session
-w /var/run/wtmp -p wa -k session
-w /var/log/btmp -p wa -k session
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Session initiation information is already being collected"
	echo
fi

# Ensuring disc acl permission modification events are collected

	echo 
	echo    "Checking to ensure disc acl permission modification events are collected.."
	echo 
if [[ -z `egrep -e "fchmod -S fchmodat" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such disc acl permission modification events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod
-a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Disc acl permission modification events are already being collected"
	echo
fi

# Ensuring unsuccessful unauthorized file access events are collected

	echo 
	echo    "Checking to ensure unauthorized file access events are collected.."
	echo 
if [[ -z `egrep -e "EACCES" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such unauthorized file access events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access
-a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=1000 -F auid!=4294967295 -k access
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Unauthorized file access events are already being collected"
	echo
fi

# Ensuring mount events are collected

	echo 
	echo    "Checking to ensure mount events are collected.."
	echo 
if [[ -z `egrep -e "-a always,exit -F arch=b64 -S mount -F" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such mount events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Such mount events are already being collected"
	echo
fi

# Ensuring file deletion events by users are collected

	echo 
	echo    "Checking to ensure file deletion events by users are collected.."
	echo 
if [[ -z `egrep -e "-a always,exit -F arch=b64 -S unlink -S unlinkat" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such file deletion events by users are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Such file deletion events by users are already being collected"
	echo
fi

# Ensuring changes to sudoers are collected

	echo 
	echo    "Checking to ensure changes to sudoers events are collected.."
	echo 
if [[ -z `egrep -e "-w /etc/sudoers -p wa -k scope" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such changes to sudoers events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Such changes to sudoers events are already being collected"
	echo
fi

# Ensuring sysadmin actions (sudolog) are collected

	echo 
	echo    "Checking to ensure sysadmin action events are collected.."
	echo 
if [[ -z `egrep -e "sudo.log" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such sysadmin action events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /var/log/sudo.log -p -wa -k actions
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Such sysadmin action events are already being collected"
	echo
fi

# Ensuring kernel module loading and unloading events are collected

	echo 
	echo    "Checking to ensure kernel module loading and unloading events are collected.."
	echo 
if [[ -z `egrep -e "-w /sbin/insmod -p x -k modules" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. such kernel module loading and unloading  events are currently not being logged!" | tee -a /root/harden.log
	echo
	echo
	echo    "Fixing this problem!"
	echo
cat <<EOT >> /etc/audit/audit.rules
-w /sbin/insmod -p x -k modules
-w /sbin/rmmod -p x -k modules
-w /sbin/modprobe -p x -k modules
-a always,exit arch=b64 -S init_module -S delete_module -k modules
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Such kernel module loading and unloading events are already being collected"
	echo
fi

# Ensuring audit configuration is immutable

	echo 
	echo    "Checking to ensure audit configuration is immutable"
	echo 
if [[ -z `egrep -e "-e 2" /etc/audit/audit.rules 2> /dev/null 2>&1` ]]; then
	echo
	echo    "Warning.. audit configuration is not immutable yet!" | tee -a /root/harden.log
	echo
	echo
e	echo    "Fixing this problem!"
 
	echo
cat <<EOT >> /etc/audit/audit.rules
-e 2
EOT
	echo
	echo    "Problem fixed!"
	echo
else
	echo
        echo    "Audit configuration is already set to immutable"
	echo
fi

cp -Rf /etc/audit/audit.rules /etc/audit/rules.d/

# Ensuring rsyslog service is started (logging)

echo
echo "Enabling rsyslog service"
echo
if systemctl list-unit-files --state=enabled -all | grep rsyslog &> /dev/null 2>&1; then
        echo
        echo "Rsyslogd is already enabled"
        echo
else
	echo
	echo "Rsyslogd is disabled or not present.." | tee -a /root/harden.log
	echo
	echo "Enabling now!"
	echo
        systemctl enable rsyslog
	echo
	echo "Done enabling rsyslogd"
	echo
fi

# Ensuring correct rsyslog permissions are set

        echo 
        echo    "Checking to ensure correct rsyslog permissions are set"
        echo 
if [[ -z `egrep -e "\$FileCreateMode 0640" /etc/rsyslog.conf 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. correct rsyslog permission missing!" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"

        echo
cat <<EOT >> /etc/rsyslog.conf
\$FileCreateMode 0640
EOT
        echo
        echo    "Problem fixed!"
        echo
else
        echo
        echo    "Correct rsyslog permissions are already set"
        echo
fi

# Ensuring rsyslog sends logs to remote host 

        echo
        echo    "Backing up original rsyslog.conf to rsyslog.conf.bkp"
        echo
                cp -f /etc/rsyslog.conf /etc/rsyslog.conf.bkp
        echo
        echo    "Backup of rsyslog config complete"
        echo
        echo
        echo    "Setting rsyslog to send all logs to syslog appliance"
        echo
        sed -i '/*.* @@remote-host:514/s/^#//g' /etc/rsyslog.conf
        sed -i -e "s/*.* @@remote-host:514/*.* @@$rsyslogip/g" /etc/rsyslog.conf
        echo
        echo    "Set rsyslog to send all logs to syslog appliance"
        echo

# Ensuring remote syslog message accepting features are disabled

	echo
	echo	"Ensuring that remote rsyslog messages are not accepted here as this is not a log host"
	echo
	echo
	echo	"Disabling remote rsyslog message accepting feature"
	echo
	sed -i 's/\$ModLoad imtcp.so//g' /etc/rsyslog.conf 2> /dev/null 2>&1
	sed -i 's/\$InputTCPServerRun 514//g' /etc/rsyslog.conf 2> /dev/null 2>&1
	sleep 2
	pkill -HUP rsyslogd
	echo	
	echo	"This host can no longer accept remote syslog messages"
	echo

# Ensuring all log files have the right permissions

	echo
        echo    "Ensuring that all log files have the right permissions set.. otherwise, setting them now"
        echo
	`find /var/log -type f -exec chmod g-wx,o-rwx {} +`
	echo
	echo	"This has been done!"
	echo

# Ensuring crond service is started (logging)

echo
echo "Enabling crond service"
echo
if systemctl list-unit-files --state=enabled -all | grep crond &> /dev/null 2>&1; then
        echo
        echo "Crond is already enabled"
        echo
else
        echo
        echo "Crond is disabled or not present.." | tee -a /root/harden.log
        echo
        echo "Enabling now!"
        echo
        systemctl enable crond
        echo
        echo "Done enabling crond"
        echo
fi

# Setting permissions on /etc/crontab

        echo
        echo "Setting correct permissions on /etc/crontab file"
        echo
        chown root:root /etc/crontab
        chmod og-rwx /etc/crontab
        echo
        echo "Correct permissions set on /etc/crontab"
        echo

# Setting permissions on /etc/crontab.hourly

        echo
        echo "Setting correct permissions on /etc/cron.hourly file"
        echo
        chown root:root /etc/cron.hourly
        chmod og-rwx /etc/cron.hourly
        echo
        echo "Correct permissions set on /etc/cron.hourly"
        echo

# Setting permissions on /etc/crontab.daily

        echo
        echo "Setting correct permissions on /etc/cron.daily file"
        echo
        chown root:root /etc/cron.daily
        chmod og-rwx /etc/cron.daily
        echo
        echo "Correct permissions set on /etc/cron.daily"
        echo

# Setting permissions on /etc/crontab.weekly

        echo
        echo "Setting correct permissions on /etc/cron.weekly file"
        echo
        chown root:root /etc/cron.weekly
        chmod og-rwx /etc/cron.weekly
        echo
        echo "Correct permissions set on /etc/cron.weekly"
        echo

# Setting permissions on /etc/crontab.monthly

        echo
        echo "Setting correct permissions on /etc/cron.weekly file"
        echo
        chown root:root /etc/cron.monthly
        chmod og-rwx /etc/cron.monthly
        echo
        echo "Correct permissions set on /etc/cron.weekly"
        echo

# Setting permissions on /etc/cron.d folder

        echo
        echo "Setting correct permissions on /etc/cron.d folder"
        echo
        chown root:root /etc/cron.d
        chmod og-rwx /etc/cron.d
        echo
        echo "Correct permissions set on /etc/cron.d folder"
        echo

# Ensuring at/cron is restricted to authorized users

	echo
	echo "Ensuring at/cron is restricted to authorized users"
	echo
	rm -rf /etc/cron.deny
	rm -rf /etc/at.deny
	touch /etc/cron.allow
	touch /etc/at.allow
	chmod og-rwx /etc/cron.allow
	chmod og-rwx /etc/at.allow
	chown root:root /etc/cron.allow
	chown root:root /etc/at.allow
	echo
	echo "Done! cron.deny and at.deny have been removed and all users who are authorized to use cron need to be listed in cron.allow & at.allow"
	echo

# Setting permissions on sshd config 

        echo
        echo "Setting correct permissions on /etc/ssh/sshd_config file"
        echo
        chown root:root /etc/ssh/sshd_config
        chmod og-rwx /etc/ssh/sshd_config
        echo
        echo "Correct permissions set on /etc/ssh/sshd_config"
        echo

# Make sure sshd is using Protocol 2 only and if not, set it to do so

	echo
	echo	"Making sure sshd is running Protocol 2"
	echo
	echo
        echo    "Backing up original sshd_config to sshd_config.bkp"
        echo
                cp -f /etc/ssh/sshd_config /etc/ssh/sshd_config.bkp
        echo
        echo    "Backup of sshd config complete"
        echo
        echo
        echo	"Setting sshd to Protocol 2"
        echo  
	sed -i '/Protocol 2/s/^#//g' /etc/ssh/sshd_config
	echo	
	echo	"Sshd protocol now set to Protocol 2!"
	echo

# Make sure sshd has loglevel set to INFO

        echo
        echo    "Making sure sshd has loglevel set to INFO"
        echo
        echo
        echo    "Setting sshd loglevel to INFO"
        echo  
        sed -i '/LogLevel INFO/s/^#//g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd loglevel now set to INFO!"
        echo

# Make sure sshd X11Forwarding is set to no

        echo
        echo    "Making sshd has X11Forwarding set to no"
        echo
        echo
        echo    "Setting sshd X11Forwarding to no"
        echo  
        sed -i -e 's/X11Forwarding yes/X11Forwarding no/g' /etc/ssh/sshd_config
        echo
	echo	"Sshd X11Forwarding now set to no!"
        echo

# Make sure sshd MaxAuthTries is set to 4

	echo
        echo    "Making sure sshd has MaxAuthTries set to 4"
        echo
        echo
        echo    "Setting sshd MaxAuthTries to 4"
        echo  
        sed -i '/MaxAuthTries 6/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's/MaxAuthTries 6/MaxAuthTries 4/g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd MaxAuthTries set to 4!"
        echo

# Make sure sshd IgnoreRhosts is set to yes

        echo
        echo    "Making sure sshd has IgnoreRhosts set to yes"
        echo
        echo
        echo    "Setting sshd IgnoreRhosts to yes"
        echo  
        sed -i '/IgnoreRhosts yes/s/^#//g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd IgnoreRhosts set to yes!"
        echo

# Make sure sshd HostbasedAuthentication is set to no

        echo
        echo    "Making sure sshd has HostbasedAuthentication set to no"
        echo
        echo
        echo    "Setting sshd HostbasedAuthentication to no"
        echo  
        sed -i '/HostbasedAuthentication no/s/^#//g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd HostbasedAuthentication set to no!"
        echo

# Make sure sshd PermitRootLogin is set to no

        echo
        echo    "Making sure sshd has PermitRootLogin set to no"
        echo
        echo
        echo    "Setting sshd PermitRootLogin to no"
        echo  
        sed -i '/PermitRootLogin yes/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd PermitRootLogin set to no!"
        echo

# Make sure sshd PermitEmptyPasswords is set to no

        echo
        echo    "Making sure sshd has PermitEmptyPasswords set to no"
        echo
        echo
        echo    "Setting sshd PermitEmptyPasswords to no"
        echo  
        sed -i '/PermitEmptyPasswords no/s/^#//g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd PermitEmptyPasswords set to no!"
        echo

# Make sure sshd PermitUserEnvironment is set to no

        echo
        echo    "Making sure sshd has PermitUserEnvironment set to no"
        echo
        echo
        echo    "Setting sshd PermitUserEnvironment to no"
        echo  
        sed -i '/PermitUserEnvironment no/s/^#//g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd PermitUserEnvironment set to no!"
        echo

# Checking and ensuring sshd has proper ciphers set

	echo
	echo	"Checking to see if sshd has proper ciphers set.."
	echo
if [[ -z `egrep -e "Ciphers aes256-ctr,aes192-ctr,aes128-ctr" /etc/ssh/sshd_config 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Proper ciphers for sshd not set!" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
cat <<EOT >> /etc/ssh/sshd_config
Ciphers aes256-ctr,aes192-ctr,aes128-ctr
EOT
        echo
        echo    "Proper ciphers now added to sshd config"
        echo
else
        echo
        echo    "Proper ciphers already exist in sshd config"
        echo
fi

# Checking and ensuring sshd has proper MACs set

	echo
	echo	"Checking to see if sshd has proper MACs set.."
	echo
if [[ -z `egrep -e "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com" /etc/ssh/sshd_config 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Proper MACs for sshd not set!" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
cat <<EOT >> /etc/ssh/sshd_config
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
EOT
        echo
        echo    "Proper MACs now added to sshd config"
        echo
else
        echo
        echo    "Proper MACs already exist in sshd config"
        echo
fi

# Make sure sshd ClientAliveInterval is set to 300

        echo
        echo    "Making sure sshd has ClientAliveInterval set to 300"
        echo
        echo
        echo    "Setting sshd ClientAliveInterval to 300"
        echo  
        sed -i '/ClientAliveInterval 0/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's/ClientAliveInterval 0/ClientAliveInterval 300/g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd ClientAliveInterval set to 300!"
        echo

# Make sure sshd ClientAliveCountMax is set to 0

        echo
        echo    "Making sure sshd has ClientAliveCountMax set to 0"
        echo
        echo
        echo    "Setting sshd ClientAliveCountMax to 0"
        echo  
        sed -i '/ClientAliveCountMax 3/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's/ClientAliveCountMax 3/ClientAliveCountMax 0/g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd ClientAliveCountMax set to 0!"
        echo

# Make sure sshd LoginGraceTime is set to 60 (seconds)

        echo
        echo    "Making sure sshd has LoginGraceTime set to 60 seconds"
        echo
        echo
        echo    "Setting sshd LoginGraceTime to 60"
        echo  
        sed -i '/LoginGraceTime 2m/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's/LoginGraceTime 2m/LoginGraceTime 60/g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd LoginGraceTime set to 60!"
        echo

# Make sure sshd Banner is set to point to /etc/issue.net

        echo
        echo    "Making sure sshd has Banner pointed to /etc/issue.net"
        echo
        echo
        echo    "Setting sshd Banner to /etc/issue.net"
        echo  
	sed -i '/Banner none/s/^#//g' /etc/ssh/sshd_config
        sed -i -e 's#Banner none#Banner /etc/issue.net#g' /etc/ssh/sshd_config
        echo    
        echo    "Sshd banner set to /etc/issue.net!"
        echo

# Setting PAM password-auth policy

        echo
        echo    "Backing up /etc/pam.d/password-auth to /etc/pam.d/password-auth.bkp"
        echo
        cp -f /etc/pam.d/password-auth /etc/pam.d/password-auth.bkp
        echo    
        echo    "Backup of password-auth complete!"
        echo    
        echo
        echo    "Making sure PAM password-auth has correct policy set"
        echo
        echo
        echo    "Setting PAM password-auth policy correctly"
        echo  
        sed -i -e 's/pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=/pam_pwquality.so try_first_pass retry=3/g' /etc/pam.d/password-auth
        echo    
        echo    "PAM password-auth policy set correctly!"
        echo

#!/bin/bash
# Setting PAM system-auth policy

        echo
        echo    "Backing up /etc/pam.d/system-auth to /etc/pam.d/system-auth.bkp"
        echo
        cp -f /etc/pam.d/system-auth /etc/pam.d/system-auth.bkp
        echo    
        echo    "Backup of system-auth complete!"
        echo    
        echo
        echo    "Making sure PAM system-auth has correct policy set"
        echo
        echo
        echo    "Setting PAM password-auth policy correctly"
        echo  
        sed -i -e 's/pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=/pam_pwquality.so try_first_pass retry=3/g' /etc/pam.d/system-auth
        echo    
        echo    "PAM system-auth policy set correctly!"
        echo

# Setting pwquality!

	echo
	echo	"Backing up /etc/security/pwquality.conf to /etc/security/pwquality.conf.bkp"
	echo	
	cp -f /etc/security/pwquality.conf /etc/security/pwquality.conf.bkp
	echo
	echo	"Backup of pwquality.conf complete"
	echo
	echo
	echo	"Setting pwquality rules!"
	echo
if [[ -z `egrep -e "minlen=14" /etc/security/pwquality.conf 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. pwquality settings not set" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
cat <<EOT >> /etc/security/pwquality.conf
minlen=14
dcredit=-1
ucredit=-1
ocredit=-1
lcredit=-1
EOT
	echo
	echo	"Pwquality rules set correctly!"
	echo
else
	echo
	echo	"Pwquality rules are already in effect!" | tee -a /root/harden.log
	echo
fi

# Setting faillock in password-auth!

echo
        echo    "Setting faillock rules in password-auth!"
        echo
if [[ -z `egrep -e "faillock.so" /etc/pam.d/password-auth 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. faillock rules not set in password-auth" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
sed -i '/auth        required      pam_deny.so/a \
auth required pam_faillock.so preauth audit silent deny=5 unlock_time=900 \
auth [success=1 default=bad] pam_unix.so \
auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900 \
auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=900' /etc/pam.d/password-auth
	echo
        echo    "Faillock rules set correctly in password-auth!"
        echo
else
        echo
        echo    "Faillock rules are already in effect in password-auth!" | tee -a /root/harden.log
        echo
fi

# Setting faillock in system-auth!

echo
        echo    "Setting faillock rules in system-auth!"
        echo
if [[ -z `egrep -e "faillock.so" /etc/pam.d/system-auth 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. faillock rules not set in system-auth" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
sed -i '/auth        required      pam_deny.so/a \
auth required pam_faillock.so preauth audit silent deny=5 unlock_time=900 \
auth [success=1 default=bad] pam_unix.so \
auth [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900 \
auth sufficient pam_faillock.so authsucc audit deny=5 unlock_time=900' /etc/pam.d/system-auth	
	echo	
        echo    "Faillock rules set correctly in system-auth!"
        echo
else
        echo
        echo    "Faillock rules are already in effect in system-auth!" | tee -a /root/harden.log
        echo
fi

# Ensuring that password reuse is limited
	echo	
        echo    "Checking to ensure password reuse is limited in password-auth"
        echo
if [[ -z `egrep -e "remember=5" /etc/pam.d/password-auth 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. password reuse rules not set in password-auth" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
sed -i '/password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok/s/$/ remember=5/' /etc/pam.d/password-auth
	echo    
        echo    "Password reuse rules set correctly in password-auth!"
        echo
else
        echo
        echo    "Password reuse rules are already in effect in password-auth!" | tee -a /root/harden.log
        echo
fi

# Ensuring that password reuse is limited in system-auth
	echo	
        echo    "Checking to ensure password reuse is limited in system-auth"
        echo
if [[ -z `egrep -e "remember=5" /etc/pam.d/system-auth 2> /dev/null 2>&1` ]]; then
        echo
        echo    "Warning.. password reuse rules not set in system-auth" | tee -a /root/harden.log
        echo
        echo
        echo    "Fixing this problem!"
        echo
sed -i '/password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok/s/$/ remember=5/' /etc/pam.d/system-auth
	echo    
        echo    "Password reuse rules set correctly in system-auth!"
        echo
else
        echo
        echo    "Password reuse rules are already in effect in system-auth!" | tee -a /root/harden.log
        echo
fi

# Setting login.defs for MAX & MIN DAYS for password change

	echo
	echo	"Backing up /etc/login.defs to /etc/login.defs.bkp"
	echo
		cp -f /etc/login.defs /etc/login.defs.bkp
	echo
	echo	"Backup of this file is completed"
	echo
	echo
	echo	"Ensuring password min/max days before expiry are set correctly"
	echo
sed -i '25s/.*/PASS_MAX_DAYS   90/' /etc/login.defs
sed -i '26s/.*/PASS_MIN_DAYS   7/' /etc/login.defs
	echo
	echo	"Password min/max days enforced correctly"
	echo

# Setting umask to 027 in /etc/bashrc and /etc/profile

	echo
        echo    "Backing up /etc/bashrc & /etc/profile to /etc/bashrc.bkp and /etc/profile.bkp respective"
        echo
		cp -f /etc/bashrc /etc/bashrc.bkp
		cp -f /etc/profile /etc/profile.bkp
	echo
        echo    "Backup of these files is completed"
        echo
        echo
        echo    "Ensuring umask is set to 027 for all user accounts in the system via these two files"
        echo
if [[ -z `egrep -e "027" /etc/profile 2> /dev/null 2>&1` ]]; then
	sed -i 's/\<umask 0.*\>/umask 027/g' /etc/bashrc
	sed -i 's/\<umask 0.*\>/umask 027/g' /etc/profile
	echo
	echo	"Umask set correctly to 027"
	echo
else
	echo	
	echo	"Umask already set to 027" | tee -a /root/harden.log
	echo
fi

# Restricting access to su command

	echo
        echo    "Setting su command restrictions in pam for su!"
        echo
        sed -i '/auth		required	pam_wheel.so use_uid/s/^#//g' /etc/pam.d/su
	echo    
        echo    "Su command restrictions set correctly in pam for su!"
        echo

# Setting permissions on passwd, group and shadow files

echo
echo "Setting permissions on passwd, group and shadow files"
echo
chown root:root /etc/passwd
chmod 644 /etc/passwd
chown root:root /etc/shadow
chmod 000 /etc/shadow
chown root:root /etc/group
chmod 644 /etc/group
chown root:root /etc/gshadow
chmod 000 /etc/gshadow
chown root:root /etc/passwd-
chmod 600 /etc/passwd-
chown root:root /etc/shadow-
chmod 600 /etc/shadow-
chown root:root /etc/group-
chmod 600 /etc/group-
chown root:root /etc/gshadow-
chmod 600 /etc/gshadow-
echo 
echo "Done! :@"
echo

echo
echo "Ensuring firewalld is turned on at boot"
echo
systemctl enable firewalld && systemctl start firewalld 
echo
echo "Firewalld is started and will be turned on at boot"
echo

sleep 5
echo
echo "HARDENING COMPLETE.. please reboot the servers to ensure all settings have taken effect ASAP"
echo
